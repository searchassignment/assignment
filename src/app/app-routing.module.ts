import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HeaderComponent} from '../app/header/header.component';
import {SecondComponent} from '../app/second/second.component';
const routes: Routes = [
  // { path:"",component:HeaderComponent},
  { path:"second",component:SecondComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
