import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {
  userdata: any;
  address: any;
  description: any;
  email: any;
  first_name; any;
  last_name: any;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    var data = this.activatedRoute.snapshot.paramMap.get('id');
    this.userdata = JSON.parse(data)
    this.address = this.userdata.address
    this.description = this.userdata.description
    this.email = this.userdata.email
    this.first_name = this.userdata.first_name
    this.last_name = this.userdata.last_name
    console.log(this.address)
    console.log(this.userdata);
  }
}
