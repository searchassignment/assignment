import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  searchdata: any;
  emailid: any;
  myReactiveForm: FormGroup
  searchResult: any;

  constructor(private http: HttpClient, private router: Router, ) { }

  ngOnInit(): void {

  }
  search(searchResult) {
    console.log(searchResult);
    this.searchResult = this.getUrlParameter('searchResult');
    console.log(this.searchResult);
    this.http.get<any>('https://ltv-data-api.herokuapp.com/api/v1/records.json?email=' + searchResult).subscribe(cntdata => {
      console.log("search data", cntdata)
      this.searchdata = cntdata
      var dt = JSON.stringify(this.searchdata);
      this.router.navigate(["second", { id: dt }]);

    });
  }

  getUrlParameter(sParam) {
    console.log(sParam)
    var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName;
    for (let i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
        //console.log(sParameterName);
      }
      console.log(sParameterName);

    }
  };

}
